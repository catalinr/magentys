package utils;

/**
 * This is a class where are put all Constants.
 */
public class Constants {
    public static final String URL = "https://www.sportsdirect.com";

    /** Default browser is firefox.
     Also it is supported Chrome but will have to copy the chromedriver.exe file to C://chromedriver.exe
     or edit the PATH in the Helper file. */
    public static final String BROWSER = "firefox";
//    public static final String BROWSER = "chrome";
}
