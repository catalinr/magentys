package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper {

    public static WebDriver openBrowser() {
        WebDriver webDriver;
        if (Constants.BROWSER.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
            webDriver = new ChromeDriver();
        } else {
            webDriver = new FirefoxDriver();
        }
        webDriver.manage().window().maximize();
        return webDriver;
    }

    public static void waitForElementToBeClickable(WebDriver webDriver, WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void moveToElement(WebDriver webDriver, WebElement element) throws InterruptedException {
        Actions driverActions = new Actions(webDriver);
        try {
            driverActions.moveToElement(element).perform();
            Thread.sleep(200);
        } catch (MoveTargetOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

}
