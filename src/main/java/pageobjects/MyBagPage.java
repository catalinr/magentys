package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represent My Bag Page / Cart page
 */
public class MyBagPage {

    private final WebDriver driver;

    public MyBagPage(WebDriver driver) {
        this.driver = driver;

        // Check that we're on the right page.
        if (!driver.getTitle().contains("Cart")) {
            throw new IllegalStateException("This is not the cart page");
        }
        //initialization of the webelements from the page
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CSS, css = "div.BagandCheckoutBtn a")
    public WebElement continueShopping;

    @FindBys({@FindBy(css = "td.productdesc a")})
    private List<WebElement> bagProductsNameList;

    public void clickContinueShoppingButton() {
        continueShopping.click();
    }

    public List<String> getProductsNameList() {
        List<String> productsNameList = new ArrayList<String>();

        //will extract the products name text from all webElements, and add them to a new list
        for (int i = 0; i < bagProductsNameList.size(); i++) {
            productsNameList.add(i, bagProductsNameList.get(i).getText());
        }
        return productsNameList;
    }
}
