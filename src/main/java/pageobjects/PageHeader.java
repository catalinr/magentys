package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * This page is mapping the Page Header, which contains buttons like: Main, All Deals, sportdirectfitness.com, Sale,
 * Help, Stores, Jobs, Register, Sign In.
 * Also it contains the Search button and search input.
 * <p/>
 * This page header can be accessed from any of the website pages.
 */

public class PageHeader {
    private WebDriver webDriver;

    public PageHeader(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(how = How.CSS, css = "input#txtSearch")
    private WebElement searchBox;

    @FindBy(how = How.CSS, css = "a#cmdSearch")
    private WebElement searchButton;

    @FindBy(how = How.CSS, css = "a#aBagLink")
    private WebElement bagButtonLink;

    public void typeSearchText(String textToSearch) {
        searchBox.sendKeys(textToSearch);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public MyBagPage clickBagButton() {
        bagButtonLink.click();
        return new MyBagPage(webDriver);
    }

    public void searchFor(String textToSearch) {
        typeSearchText(textToSearch);
        clickSearchButton();
    }

}
