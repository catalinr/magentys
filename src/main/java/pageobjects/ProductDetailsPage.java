package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * This class represent the product details page.
 * <p/>
 * For the moment, there are not mapped all the functionalities, only Add To Bag button
 */
public class ProductDetailsPage {
    private WebDriver webDriver;

    public ProductDetailsPage(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CSS, css = "a.addToBag")
    public WebElement addToBagButton;

    public void clickAddToBagButton() {
        addToBagButton.click();
    }
}
