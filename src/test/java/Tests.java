import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageobjects.MyBagPage;
import pageobjects.PageHeader;
import pageobjects.ProductDetailsPage;
import utils.Constants;
import utils.Helper;

public class Tests {
    PageHeader pageHeader;
    ProductDetailsPage productDetailsPage;
    MyBagPage myBagPage;
    WebDriver webDriver;

    public String UNIQUE_PRODUCT_TO_SEARCH = "Karrimor 22b Running Sun Glasses"; //this product should be unique on search results

    @BeforeMethod
    void openBrowser() {
        webDriver = Helper.openBrowser();
        webDriver.get(Constants.URL);
    }

    @Test
    public void addUniqueProductToBagTest() throws InterruptedException {

        pageHeader = new PageHeader(webDriver);
        pageHeader.searchFor(UNIQUE_PRODUCT_TO_SEARCH);

        //check if it is opened the expected product details page
        if (!webDriver.getTitle().contains(UNIQUE_PRODUCT_TO_SEARCH)) {
            throw new IllegalStateException("This is not the expected product details page");
        }

        productDetailsPage = new ProductDetailsPage(webDriver);

        Helper.waitForElementToBeClickable(webDriver, productDetailsPage.addToBagButton);
        productDetailsPage.clickAddToBagButton();

        //initialization of the elements is done in the page constructor here, in order to have a new method of work
        myBagPage = pageHeader.clickBagButton();

        //assert that the product is added to the bag
        Assert.assertTrue(myBagPage.getProductsNameList().toString().contains(UNIQUE_PRODUCT_TO_SEARCH));
    }

    @AfterMethod
    void closeBrowser() {
        webDriver.close();
    }
}
